
# ochatemplates

<!-- README.md is generated from README.Rmd. Please edit that file -->

[![GitLab CI Build
Status](https://gitlab.com/dickoa/ochatemplates/badges/master/build.svg)](https://gitlab.com/dickoa/ochatemplates/pipelines)
[![Codecov Code
Coverage](https://codecov.io/gl/dickoa/ochatemplates/branch/master/graph/badge.svg)](https://codecov.io/gl/dickoa/ochatemplates)
[![License:
MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)

`ochatemplates` is a package that provide a set of `rmarkdown` templates
inspired by OCHA graphics stylebook. The goal of this package to allow R
user to easily produce reproducible humanitarian data products.

## Installation

This package is not on yet on CRAN and to install it, you will need the
[`remotes`](https://github.com/r-lib/remotes) package.

``` r
## install.packages("remotes") 
remotes::install_gitlab("dickoa/ochatemplates")
```

You will find in this a package:

  - OCHA template for dashboard based on `flexdashboard`:
    `ochatemplates::dashboard`
  - OCHA template for digital report (html):
    `ochatemplates::digital_report`
  - OCHA word template `ochatemplates::word_doc`

WIP:

  - Use the `pagedown::paged` instead of `html_document` for digital
    report to easily paginate an export as high quality PDF
  - Add `ochatemplates::powerpoint_pres`
  - Add `ochatemplates::pdf_doc` using LaTeX

## Meta

  - Please [report any issues or
    bugs](https://gitlab.dickoa/ochatemplates/issues).
  - License: MIT
  - Please note that this project is released with a [Contributor Code
    of Conduct](CONDUCT.md). By participating in this project you agree
    to abide by its terms.
